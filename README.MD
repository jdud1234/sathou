#English description here
https://sites.google.com/view/sathou/main

# How to Install
1. Install Substance Automation Tool-kit into PC
2. Install .whl file in $SAT install folder$/Python API. Check following page to see how to install new package into Houdini python evironment. http://wordpress.discretization.de/houdini/home/advanced-2/installing-and-using-scipy-in-houdini/ 
3. Download or clone git repository.
4. Edit SATHOU.json file. 
5. Move edited SATHOU.json file into $HOME/houdinix.y/packages.

# Pros of SAT in Houdini
1. Many parameters are automatically filed when Low Poly parameter is filled.
2. Easy to add and remove a new baker, and to link/unlink with the root node.
3. Some features are added which are not supported in other bakers.
    1. SP: Some bakers missing.
    2. SD: Per Material bake is not supported by default.
    3. SideFX Labs: Bake by Mesh Name feature missing.

# Warning
1. Baker of SideFXLabs does not require to make .FBX file, but this tool does. You should make .FBX file first.
2. Attribute bake in SideFXLabs does not support.
    To perform this, following these steps.
    1. change the name of an attribute which will be baked to Cd. 
    2. Promote Cd attribute from original class to vertex class.
    3. Export .FBX file.
    4. Use color ID bake with Vertex Color mode.
    5. If you want to bake multiple attribute, do this step as many as you need.
    6. Curretly working on automate this steps.
3. Some baker requried tangent space normal map. If the bakers are added with previous normal map bake, it will be automatically added. 
4. This tool only requires SAT license. Howver Houdini requires paid license to export .FBX file.


# Under Dev
1. Context switching from OBJ to TOP.


# 설치 방법
1. Substance Automation Tool-kit을 설치합니다.
2. http://wordpress.discretization.de/houdini/home/advanced-2/installing-and-using-scipy-in-houdini/ 이 페이지를 참고하여 <SAT 설치 폴더>/Python API 폴더의 whl 파일을 후디니 파이썬 환경에 설치합니다.
3. 깃 저장소를 다운 받거나, 클론합니다.
4. SATHOU.json 파일을 수정합니다.
5. SATHOU.json 파일을 $HOME/houdinix.y/packages로 이동시킵니다.

# SAT in Houdini의 장점
1. 다수의 파라미터가 Low Poly 파라미터 입력 시 자동으로 입력됩니다.
2. 베이커 추가/삭제가 간편하고, 루트 노드와의 연결을 자유롭게 끊었다가 다시 연결할 수 있습니다.
3. 각종 베이커에서 불가능한 여러 기능이 추가되어 있습니다.
    1. SP: 일부 베이커 없음
    2. SD: Per Material 기능 없음
    3. SideFX Labs: Bake by Mesh Name 없음

# 주의사항
1. SideFXLabs의 베이커는 SOP을 바로 베이크에 사용할 수 있지만, SAT in Houdini는 .FBX 파일을 먼저 만들어야 합니다.
2. SideFXLabs의 어트리뷰트 베이크는 SAT in Houdini에서 지원하지 않습니다. 다만, 베이크 하고 싶은 어트리뷰트를 @Cd 어트리뷰트로 바꾼 뒤, 어트리뷰트 마다 별도의 하이폴 .FBX 파일을 만들어 Color ID-Vertex Color 모드로 베이크 시 동일한 이미지를 얻을 수 있습니다.
3. 일부 베이커 추가 시, 이전에 추가된 노말 맵 베이커가 없다면, 자동으로 추가됩니다. 정상적인 작동이며, 출력된 베이크 이미지의 품질을 위해 이렇게 설정되어 있습니다.
4. .FBX 파일을 후디니에서 얻기 위해선, 유료 라이센스가 필요합니다. 결국 이 툴을 사용한다 해도 모든 일을 후디니 안에서 해결하려면 두 프로그램의 라이센스가 모두 필요합니다.
5. .FBX 파일 제작 시엔 최상위 루트 디렉토리 안에 Geo 노드가 있는 형태로 익스포트 해야 합니다. 그렇지 않으면 에러가 발생합니다.

# 개발중인 것
1. 주의사항 2번과 관계된 것으로, 어트리뷰트 베이크를 위해 어트리뷰트를 @Cd로 변경한 후 .FBX 파일로 일괄 익스포트하는 기능을 제작 중입니다.
2. Obj 네트워크가 아니라 TOP 네트워크에서 사용할 수 있는 노드를 제작 중입니다.