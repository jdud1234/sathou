import hou
from re import compile
from subprocess import PIPE
from pysbs import batchtools as bt


__normal_maps = dict()


class AtBaker:
    __node_at_dict: dict = dict()

    @classmethod
    def get_attributes(cls):
        this = hou.pwd()
        target_sop = hou.node(this.parm("_sop").eval())
        geo = target_sop.geometry()
        v_at = geo.vertexAttribs()
        v_at_name = [a.name() for a in v_at]
        AtBaker.__node_at_dict[this] = v_at_name

    @classmethod
    def set_at2bake(cls):
        this = hou.pwd()
        at2bake = this.parm('_at2bake')
        v_at_name = AtBaker.__node_at_dict[this]
        s = ' '.join(v_at_name)
        at2bake.set(s)

    @classmethod
    def get_this_ats(cls, node):
        return cls.__node_at_dict[node]


def bake_non_mesh(baker, wait: bool):
    op = getAllParms_non_mesh()
    extern_norm = hou.pwd().parm("_extern_norm").eval()
    per_mat = hou.pwd().parm("_per_mat").eval()
    if per_mat:
        if extern_norm:
            bake_all_udims_extnorm_permat(op, baker, wait)
        else:
            bake_all_udims_permat(op, baker, wait)
    else:
        if extern_norm:
            bake_all_udims_extnorm(op, baker, wait)
        else:
            bake_all_udims(op, baker, wait)


def bake(baker, wait: bool):
    op = getAllParms()
    extern_norm = hou.pwd().parm("_extern_norm").eval()
    per_mat = hou.pwd().parm("_per_mat").eval()
    if per_mat:
        if extern_norm:
            bake_all_udims_extnorm_permat(op, baker, wait)
        else:
            bake_all_udims_permat(op, baker, wait)
    else:
        if extern_norm:
            bake_all_udims_extnorm(op, baker, wait)
        else:
            bake_all_udims(op, baker, wait)


def make_normal_paths():
    this = hou.pwd()
    root = this.parent().parent()

    folder = this.parm("output_path").eval()
    name = this.parm("output_name").eval()
    ext = this.parm("output_format").eval()

    s_format = folder + "/" + name
    permat = this.parm("_per_mat").eval()

    udims = this.parm("_udim").eval().split(" ")
    udims.sort()

    if permat:
        s_format = s_format + "_%s_%s"
    else:
        s_format = s_format + "_%s"

    s_format = s_format + "." + ext

    normal_paths = dict()

    if permat:
        mats = this.parm("_materials").eval().split(" ")
        mats.sort()
        for m in mats:
            m_dict = dict()
            normal_paths[m] = m_dict
            for udim in udims:
                m_dict[udim] = s_format % (m, udim)
    else:
        for udim in udims:
            normal_paths[udim] = s_format % udim

    normal_format = this.parm("normal_format").eval()

    normal_paths['normal_format'] = normal_format

    __normal_maps[root] = normal_paths


def bake_all_udims(op, baker, wait):
    n = hou.pwd()
    udims = n.parm("_udim").eval().split(" ")
    output_name = n.parm("output_name").eval() + "_%s"

    for udim in udims:
        op['udim'] = udim
        op["output_name"] = output_name % udim
        result = baker(**op)
        if wait:
            result.wait()


def bake_all_udims_permat(op, baker, wait):
    n = hou.pwd()
    root = n.parent().parent()

    mat_mesh_dict = FBXInfo.get_mat_mesh_dict(root)
    mat_keys = mat_mesh_dict.keys()

    udims = n.parm("_udim").eval().split(" ")
    mats = n.parm("_materials").eval().split(" ")

    output_name = n.parm("output_name").eval() + "_%s_%s"

    for m in mats:
        if m not in mat_keys:
            continue
        op["input_selection"] = mat_mesh_dict[m]
        for udim in udims:
            op['udim'] = udim
            op['output_name'] = output_name % (m, udim)
            result = baker(**op)
            if wait:
                result.wait()


def bake_all_udims_extnorm(op, baker, wait):
    n = hou.pwd()
    root = n.parent().parent()
    ext_norm_paths = __normal_maps[root]
    normal_format = ext_norm_paths["normal_format"]

    op["normal_format"] = normal_format

    udims = n.parm("_udim").eval().split(" ")
    output_name = n.parm("output_name").eval() + "_%s"

    for udim in udims:
        op['udim'] = udim
        op["output_name"] = output_name % udim
        op["normal"] = ext_norm_paths[udim]
        result = baker(**op)
        if wait:
            result.wait()


def bake_all_udims_extnorm_permat(op, baker, wait):
    n = hou.pwd()
    root = n.parent().parent()
    ext_norm_paths = __normal_maps[root]
    normal_format = ext_norm_paths["normal_format"]

    op["normal_format"] = normal_format

    mat_mesh_dict = FBXInfo.get_mat_mesh_dict(root)
    mat_keys = mat_mesh_dict.keys()

    udims = n.parm("_udim").eval().split(" ")
    mats = n.parm("_materials").eval().split(" ")

    output_name = n.parm("output_name").eval() + "_%s_%s"

    for m in mats:
        if m not in mat_keys:
            continue
        op["input_selection"] = mat_mesh_dict[m]
        for udim in udims:
            op['udim'] = udim
            op['output_name'] = output_name % (m, udim)
            op['normal'] = ext_norm_paths[m][udim]
            result = baker(**op)
            if wait:
                result.wait()


def getAllParms_non_mesh():
    op = {'stdout': PIPE, 'shell': True}
    n = hou.pwd()
    w = n.parm("_w").eval()
    h = n.parm("_h").eval()
    wh = [w, h]
    op['output_size'] = wh

    parms = n.parms()
    for p in parms:
        if p.name().startswith("_"):
            continue
        if p.isDisabled():
            continue
        parm_template = p.parmTemplate().type()
        if parm_template == hou.parmTemplateType.Toggle:
            op[p.name()] = bool(p.eval())
        elif parm_template == hou.parmTemplateType.Int or \
                parm_template == hou.parmTemplateType.Float or \
                parm_template == hou.parmTemplateType.String:
            op[p.name()] = p.eval()
        else:
            continue
    return op


def getAllParms():
    op = {'stdout': PIPE, 'shell': True}
    n = hou.pwd()
    w = n.parm("_w").eval()
    h = n.parm("_h").eval()
    wh = [w, h]
    op['match'] = n.parm('_match').eval()
    op['output_size'] = wh

    parms = n.parms()
    for p in parms:
        if p.name().startswith("_"):
            continue
        if p.isDisabled():
            continue
        parm_template = p.parmTemplate().type()
        if parm_template == hou.parmTemplateType.Toggle:
            op[p.name()] = bool(p.eval())
        elif parm_template == hou.parmTemplateType.Int or \
                parm_template == hou.parmTemplateType.Float or \
                parm_template == hou.parmTemplateType.String:
            op[p.name()] = p.eval()
        else:
            continue
    return op


class FBXInfo:
    __node_mesh_dict: dict = dict()

    @classmethod
    def pop_node(cls, node):
        if cls.is_exist(node):
            cls.__node_mesh_dict.pop(node)

    @classmethod
    def is_exist(cls, node):
        return node in cls.__node_mesh_dict.keys()

    @classmethod
    def get_materials(cls, node):
        return [m.label for m in cls.__node_mesh_dict[node].materials]

    @classmethod
    def get_UV_sets(cls, node):
        return [i for i in cls.__node_mesh_dict[node].uv_set]

    @classmethod
    def get_UDIMs(cls, node):
        return [udim for udim in cls.__node_mesh_dict[node].udims]

    @classmethod
    def get_mesh_names(cls, node):
        return cls.__node_mesh_dict[node].mesh_names

    @classmethod
    def get_mat_mesh_dict(cls, node):
        return cls.__node_mesh_dict[node].mat_mesh_dict

    def __init__(self, mesh_path, node):
        if mesh_path == "":
            return

        self.__mat_mesh_dict = dict()

        path_splits = mesh_path.split('/')
        mesh_name = path_splits.pop().split('.')[0]

        file_path = '/'.join(path_splits) + "/%s_info.txt"
        file_path = file_path % mesh_name

        info_file = open(file_path, "w")
        info_kwargs = {'shell': True, 'stdout': info_file}
        infoget = bt.sbsbaker_info(mesh_path, **info_kwargs)
        infoget.wait()
        info_file.close()

        file = open(file_path)
        file.readline()
        splits = file.readlines()
        file.close()

        meshes = []
        to_append = meshes
        for s in splits:
            new_split = s[2:]
            if new_split.startswith("Entity"):
                new_list = []
                meshes.append(new_list)
                to_append = new_list
            to_append.append(new_split)
        mesh_info = [MeshInfo(x, self) for x in meshes]
        self.__mesh_info = {i.label: i for i in mesh_info}
        temp_uvset = [set([c.set_number for c in i.uv_set]) for i in mesh_info]
        uv_sets = set()
        for s in temp_uvset:
            uv_sets |= s

        self.__uv_sets = uv_sets

        temp_uvtiles = set()
        temp_udims = set()
        temp_mats = set()
        for i in mesh_info:
            for s in i.uv_set:
                for t in s.uv_tiles:
                    temp_uvtiles.add(t)
                for u in s.udim_tiles:
                    temp_udims.add(u)

            for m in i.materials:
                temp_mats.add(m)

        temp_uvtiles_list = list(temp_uvtiles)
        temp_udims_list = list(temp_udims)
        temp_mats_list = list(temp_mats)

        temp_udims_list.sort()
        temp_uvtiles_list.sort()
        temp_mats_list.sort()

        self.__uv_tiles = tuple(temp_uvtiles_list)
        self.__udims = tuple(temp_udims_list)
        self.__materials = tuple(temp_mats_list)

        FBXInfo.__node_mesh_dict[node] = self

    @property
    def mat_mesh_dict(self):
        return self.__mat_mesh_dict

    @property
    def mesh_names(self):
        return self.__mesh_info.keys()

    @property
    def mesh_info(self):
        return self.__mesh_info

    @property
    def uv_set(self):
        return self.__uv_sets

    @property
    def uv_tiles(self):
        return self.__uv_tiles

    @property
    def udims(self):
        return self.__udims

    @property
    def materials(self):
        return self.__materials


class MeshInfo:
    def __init__(self, info_list: list, fbx_info):
        p = compile(r'\d+\.\d+|-\d+\.\d+|\d+|-\d+')
        __label = "Label: "
        __enabled = "Enabled: "
        __bbox = "Bounding box:"
        __trans = "Location"
        __mesh = "Mesh:"
        __mat = "Submesh"
        __uv = "UV"
        self.__uv_set = []
        self.__materials = []
        mm_dict = fbx_info.mat_mesh_dict
        label = None
        while len(info_list) != 0:
            c = info_list.pop(0)
            if c.find(__label) >= 0:
                temp = c.replace(__label, "").strip()
                label = temp
                self.__label = temp
            elif c.find(__enabled) >= 0:
                temp = c.replace(__enabled, "").strip()
                self.__enabled = bool(int(temp))
            elif c.find(__bbox) >= 0:
                bbox_info = [info_list.pop(0), info_list.pop(0), info_list.pop(0), info_list.pop(0)]
                self.__BBox = BBox([(p.findall(i)) for i in bbox_info])

            elif c.find(__trans) >= 0:
                transform = [info_list.pop(0), info_list.pop(0)]
                info = [p.findall(i) for i in transform]
                self.__local_transform = Matrix4x4(info[0])
                self.__global_transform = Matrix4x4(info[1])
            elif c.find(__mesh) >= 0:
                info_list.pop(0)
                info_list.pop(0)
            elif c.find(__uv) >= 0:
                uv_set_number = int(p.findall(c)[0])
                unit_square = bool(p.findall(info_list.pop(0))[0])
                n_point = int(p.findall(info_list.pop(0))[0])
                uv_tiles = tuple(info_list.pop(0).replace("UV tiles : ", "").strip().split(" "))
                udim_tiles = tuple(info_list.pop(0).replace("Udim tiles : ", "").strip().split(" "))
                int_udim_tiles = [int(i) for i in udim_tiles]
                int_udim_tiles.sort()
                self.__uv_set.append(UVSet(uv_set_number, unit_square, n_point, uv_tiles, int_udim_tiles))
            elif c.find(__mat) >= 0:
                mat_label = info_list.pop(0).replace("Label ", "").strip()
                mat_color = tuple([float(f) for f in p.findall(info_list.pop(0))])
                n_tri = int(p.findall(info_list.pop(0))[0])
                self.__materials.append(Material(mat_label, mat_color, n_tri))
                if mat_label not in mm_dict.keys():
                    mm_dict[mat_label] = []
                mm_dict[mat_label].append(label)

    @property
    def label(self):
        return self.__label

    @property
    def uv_set(self):
        return self.__uv_set

    @property
    def materials(self):
        return self.__materials

    @property
    def enabled(self):
        return self.__enabled

    @property
    def bbox(self):
        return self.__BBox

    @property
    def local_transform(self):
        return self.__local_transform

    @property
    def global_transform(self):
        return self.__global_transform


class BBox:
    def __init__(self, info_list):
        self.min_point = tuple([float(i) for i in info_list[0]])
        self.max_point = tuple([float(i) for i in info_list[1]])
        self.center = tuple([float(i) for i in info_list[2]])
        self.size = tuple([float(i) for i in info_list[3]])


class Matrix4x4:
    def __init__(self, matrix_array):
        m = [[], [], [], []]
        r = range(4)
        for i in r:
            for j in r:
                m[i].append(float(matrix_array[i * 4 + j]))
        self.matrix = (tuple(m[0]), tuple(m[1]), tuple(m[2]), tuple(m[3]),)

    def __getitem__(self, x, y):
        return self.matrix[x][y]


class UVSet:
    def __init__(self, set_number, unit_square, n_point, uv_tiles, udim_tiles):
        self.__set_number = set_number
        self.__unit_square = unit_square
        self.__n_points = n_point
        self.__uv_tiles = uv_tiles
        self.__udim_tiles = udim_tiles

    @property
    def set_number(self):
        return self.__set_number

    @property
    def unit_square(self):
        return self.__unit_square

    @property
    def n_points(self):
        return self.__n_points

    @property
    def uv_tiles(self):
        return self.__uv_tiles

    @property
    def udim_tiles(self):
        return self.__udim_tiles


class Material:
    def __init__(self, label, colors, n_tri):
        self.__label = "default" if label == "" else label
        self.__color = colors
        self.__number_of_triangles = n_tri
        self.__mesh_set = set()

    def __lt__(self, other):
        return self.label < other.label

    def add_mesh(self, mesh):
        self.__mesh_set.add(mesh)

    @property
    def mesh_set(self):
        return tuple(self.__mesh_set)

    @property
    def label(self):
        return self.__label

    @property
    def color(self):
        return self.__color

    @property
    def number_of_triangles(self):
        return self.__number_of_triangles


class Root:
    __bakers = {
        "normal_from_mesh::1.0": (True, False,),
        "world_normal_from_mesh::1.0": (False, False,),
        "opacity_mask::1.0": (False, False,),
        "height_from_mesh::1.0": (False, False,),
        "color_map::1.0": (False, False,),
        "position_from_mesh::1.0": (False, False,),
        "uv_map::1.0": (False, False,),
        "bent_normal_from_mesh::1.0": (False, False,),
        "ao_from_mesh::1.0": (False, True,),
        "curvature_from_mesh::1.0": (False, True,),
        "thickness_from_mesh::1.0": (False, False,)
    }

    @staticmethod
    def clear():
        this = hou.pwd()
        bakers = this.node("bakers")
        c = bakers.children()
        for n in c:
            n.destroy()

    @staticmethod
    def add_all():
        for k, v in Root.__bakers.items():
            Root.addBaker(k, v[0], v[1])

    @staticmethod
    def openFolder():
        import os
        path = os.path.realpath(hou.pwd().parm("output_path").eval())
        os.startfile(path)

    @staticmethod
    def getMeshInfo():
        this = hou.pwd()
        low_path = this.parm("inputs").eval()
        if low_path == "":
            return
        FBXInfo(low_path, this)
        this.parm("uv_set").set(0)
        name = low_path.split("/")[-1].split(".")[0]

        low_suffix = this.parm("name_suffix_low").eval()
        name = name.replace(low_suffix, "")

        this.parm("output_name").set(name)
        this.parm("output_path").set('$HIP/' + name)
        Root.resetMeshNames()
        Root.resetUdim()
        Root.resetMat()

    @staticmethod
    def resetMeshNames():
        this = hou.pwd()
        mesh_names = " ".join(FBXInfo.get_mesh_names(this))
        this.parm("_meshes").set(mesh_names)

    @staticmethod
    def resetUdim():
        this = hou.pwd()
        udim = " ".join([str(i) for i in FBXInfo.get_UDIMs(this)])
        this.parm("_udim").set(udim)

    @staticmethod
    def resetMat():
        this = hou.pwd()
        mats = FBXInfo.get_materials(this)
        mat_set = list(set(mats))
        mat_set.sort()
        this.parm("_materials").set(" ".join(mat_set))

    @staticmethod
    def bakeAll():
        this = hou.pwd()
        refresh = this.parm("_ref_b4_bake").eval()
        if refresh:
            Root.getMeshInfo()
        bakers_parm = this.parm("bakers")
        curr_count = bakers_parm.eval()
        r = range(1, curr_count + 1)
        for i in r:
            path = this.parm("b_%d" % i).eval()
            baker_node = hou.node(path)
            if baker_node.isBypassed():
                continue
            button = baker_node.parm("bake")
            button.pressButton()

    @staticmethod
    def addBaker(baker_name, normal, require_prev_norm):
        f = "b_%d"
        this = hou.pwd()
        node = hou.pwd().node("bakers")
        is_normal = normal

        bakers_parm = this.parm("bakers")

        if require_prev_norm:
            bakers_number = bakers_parm.eval()
            prev_norm = None
            r = range(1, bakers_number + 1)
            for i in r:
                path = this.parm(f % i).eval()
                n = hou.node(path)
                norm = n.parm("_NORM")
                if norm is not None:
                    prev_norm = norm
                    break

            if prev_norm is None:
                to_add = "normal_from_mesh::1.0"
                tp = Root.__bakers[to_add]
                Root.addBaker(to_add, tp[0], tp[1])

        baker = node.createNode(baker_name)

        curr_count = bakers_parm.eval()
        bakers_parm.set(curr_count + 1)
        if is_normal:
            prev_parm = this.parm(f % 1)
            new_parm = this.parm(f % (curr_count + 1))

            prev_str = prev_parm.eval()
            new_parm.set(prev_str)

            prev_parm.set(baker.path())
        else:
            new_parm = this.parm(f % (curr_count + 1))
            new_parm.set(baker.path())

        node.layoutChildren()


class Hardedge:

    @staticmethod
    def find_hardedge(node):

        geo = node.geometry()
        hda = node.parent()

        make_hard = hda.parm('makehard').eval()

        if make_hard:
            edgegroup = geo.createEdgeGroup(hda.parm('groupname').eval())
        hard = edgegroup

        make_us = hda.parm('makeusgroup').eval()
        if make_us:
            if hda.parm('include').eval():
                unshared = edgegroup
            else:
                unshared = geo.createEdgeGroup(hda.parm('unshared').eval())

        soft = hda.parm('soft').eval()
        softedge = None
        if soft:
            softedge = geo.createEdgeGroup(hda.parm('softgroup').eval())

        pattern = hda.parm('edgeGroup').eval()
        if pattern.strip() == '':
            pattern = '*'

        for edge in geo.globEdges(pattern):
            edgePoints = edge.points()
            edgePrims = edge.prims()
            numprims = len(edgePrims)

            if numprims == 1:
                if make_us:
                    unshared.add(edge)
                continue

            ptvert = []
            for p in edgePoints:
                for v in p.vertices():
                    ptvert.append(v)

            prim1_vert = edgePrims[0].vertices()
            prim2_vert = edgePrims[1].vertices()

            net_vert1 = []
            for v in prim1_vert:
                if v in ptvert:
                    net_vert1.append(v)

            net_vert2 = []
            for v in prim2_vert:
                if v in ptvert:
                    net_vert2.append(v)

            p1 = net_vert1[0].point()

            p1vtx = [net_vert1[0]]
            p2vtx = [net_vert1[1]]

            for v in net_vert2:
                if v.point() == p1:
                    p1vtx.append(v)
                    continue
                else:
                    p2vtx.append(v)
                    continue

            norms1 = set([v.attribValue('N') for v in p1vtx])
            norms2 = set([v.attribValue('N') for v in p2vtx])
            n1 = len(norms1)
            n2 = len(norms2)

            if n1 + n2 > 2:
                if make_hard:
                    hard.add(edge)
            elif soft:
                softedge.add(edge)
